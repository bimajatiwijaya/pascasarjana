import requests
from bs4 import BeautifulSoup
import re
import unicodedata
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import editdistance

"""
pattern google
Content  : string class "rc"

1. Siapkan list string yang berarti bima jati wijaya
2. Kunjungi semua link google.co.id
3. Parse document hasil pencarian google dan compare dengan list poin 1
    1. Jika ada satu atau lebih string poin 1 pada dokumen nomor 3 program 
       asumsikan dokumen tersebut valid
    2. Jika ada string yang mirip2 cek similarity menggunakan levenstein 
       jika hasil kemiripan diatas atau sama dengan 75% -> mungkin benar dan 
       selainya dianggap tidak relevan.
    3. panjang (string a) - diff / string a * 100%
    Misal :
    1. bimajatiwijaya vs bimajatiwiguna memiliki perbedaan 0 artinya

4. laporan akhir muncul table sebagai berikut
    total pencarian nama    : x halaman
    total pencarian valid   : y halaman
    total mungkin valid     : z halaman
    total tidak valid       : a halaman
    Tingkat presisi         : z div x * 100% 
"""
library_ina = \
    ['yang', 'untuk', 'pada', 'ke', 'para', 'namun', 'menurut', 'antara',
     'dia', 'dua', 'ia', 'seperti', 'jika', 'jika', 'sehingga', 'kembali',
     'dan', 'tidak', 'ini', 'karena', 'kepada', 'oleh', 'saat', 'harus',
     'sementara', 'setelah', 'belum', 'kami', 'sekitar', 'bagi', 'serta',
     'di', 'dari', 'telah', 'sebagai', 'masih', 'hal', 'ketika', 'adalah',
     'itu', 'dalam', 'bisa', 'bahwa', 'atau', 'hanya', 'kita', 'dengan',
     'akan', 'juga', 'ada', 'mereka', 'sudah', 'saya', 'terhadap', 'secara',
     'agar', 'lain', 'anda', 'begitu', 'mengapa', 'kenapa', 'yaitu', 'yakni',
     'daripada', 'itulah', 'lagi', 'maka', 'tentang', 'demi', 'dimana',
     'kemana', 'pula', 'sambil', 'sebelum', 'sesudah', 'supaya', 'guna', 'kah',
     'pun', 'sampai', 'sedangkan', 'selagi', 'sementara', 'tetapi', 'apakah',
     'kecuali', 'sebab', 'selain', 'seolah', 'seraya', 'seterusnya', 'tanpa',
     'agak', 'boleh', 'dapat', 'dsb', 'dst', 'dll', 'dahulu', 'dulunya', 'anu',
     'demikian', 'tapi', 'ingin', 'juga', 'nggak', 'mari', 'nanti',
     'melainkan', 'oh', 'ok', 'seharusnya', 'sebetulnya', 'setiap',
     'setidaknya', 'sesuatu', 'pasti', 'saja', 'toh', 'ya', 'walau', 'tolong',
     'tentu', 'amat', 'apalagi', 'bagaimanapun']


def stop_word_remover(text, library_ina):
    """Remove stop words."""
    words = text.split(' ')
    for word in words:
        if word in library_ina:
            words.remove(word)
    return ' '.join(words)

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()
q_clean = 'bimajatiwijayaudinus2015'
q_len = len(q_clean)
q = 'bimajatiwijaya+udinus+2015'
# Get google with my name
r = requests.session()
main_url = 'https://www.google.co.id/'
head_url = 'https://www.google.co.id/search?biw=1329&bih=678&ei=5abRWoOWEYqFvQSupbngBA&q=' + q + ''
# head_url = 'https://www.google.co.id/search?biw=1329&bih=678&ei=jczIWqSUHor9vgTVxpW4DQ&q='+q+''
response = r.get(head_url, stream=True)
soup = BeautifulSoup(response.text, 'html.parser')
# print(soup.prettify())
# print(len(soup.select("a[href*=search?q=" + q + "]")))
set_url = {}
# for i in soup.select("a[href*=search?q=bimajatiwijaya+udinus+2015]"):
#     print(i.get(key='href'))
for i in soup.findAll("a", {"class": "fl"}):
    set_url[i.get(key='href')] = False

next_url = set_url.copy()
history_url = set_url.copy()
relevan = {}
total_relevan = 0
total_url = 0
while True:
    # SPLIT based <div class="g"> mean 1 document
    for i in soup.prettify().split('<div class="g">'):
        element = BeautifulSoup(i, 'html.parser')
        tab = re.sub('\s+', '', element.text)
        tab = unicodedata.normalize('NFKD', tab).encode('ascii', 'ignore')
        tab = stemmer.stem(tab)
        tab = stop_word_remover(tab, library_ina)
        print (tab)  # WITHOUT TAG html
        i = 0
        tab_len = len(tab)
        max = tab_len - 1
        similar = False
        next = i + q_len
        while next < max:
            next = i + q_len
            # to show result please remove comment on code bellow
            # print(b[i:next], " vs ", q, "= ", editdistance.eval(b[i:next], q))
            diff = editdistance.eval(tab[i:next], q_clean)
            if ((q_len - diff) / float(q_len)) * 100 >= 75.0:
                similar = True
                break
        if similar:
            total_relevan += 1
        total_url += 1
    set_url = {}
    for i in soup.findAll("a", {"class": "fl"}):
        set_url[i.get(key='href')] = False
    if not set_url:
        break
    next_url = set_url.popitem()[0]
    response = r.get(main_url + next_url, stream=True)
    soup = BeautifulSoup(response.text, 'html.parser')

print ("total")
"""
Library
1. https://en.wikipedia.org/wiki/Levenshtein_distance or cosine similarity ?
2. https://github.com/har07/PySastrawi
3. BeautifulSoup
Flow checking string similarity
('hardware bima ja', ' vs ', 'bima jati wijaya', '= ', 12L)
('ardware bima jat', ' vs ', 'bima jati wijaya', '= ', 13L)
('rdware bima jati', ' vs ', 'bima jati wijaya', '= ', 12L)
('dware bima jati ', ' vs ', 'bima jati wijaya', '= ', 12L)
('ware bima jati w', ' vs ', 'bima jati wijaya', '= ', 10L)
('are bima jati wi', ' vs ', 'bima jati wijaya', '= ', 8L)
('re bima jati wij', ' vs ', 'bima jati wijaya', '= ', 6L)
('e bima jati wija', ' vs ', 'bima jati wijaya', '= ', 4L)
(' bima jati wijay', ' vs ', 'bima jati wijaya', '= ', 2L)
('bima jati wijaya', ' vs ', 'bima jati wijaya', '= ', 0L)
('ima jati wijaya ', ' vs ', 'bima jati wijaya', '= ', 2L)
('ma jati wijaya r', ' vs ', 'bima jati wijaya', '= ', 4L)
"""