from bs4 import BeautifulSoup
import re
import unicodedata
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import editdistance
import datetime

library_ina = \
    ['yang', 'untuk', 'pada', 'ke', 'para', 'namun', 'menurut', 'antara',
     'dia', 'dua', 'ia', 'seperti', 'jika', 'jika', 'sehingga', 'kembali',
     'dan', 'tidak', 'ini', 'karena', 'kepada', 'oleh', 'saat', 'harus',
     'sementara', 'setelah', 'belum', 'kami', 'sekitar', 'bagi', 'serta',
     'di', 'dari', 'telah', 'sebagai', 'masih', 'hal', 'ketika', 'adalah',
     'itu', 'dalam', 'bisa', 'bahwa', 'atau', 'hanya', 'kita', 'dengan',
     'akan', 'juga', 'ada', 'mereka', 'sudah', 'saya', 'terhadap', 'secara',
     'agar', 'lain', 'anda', 'begitu', 'mengapa', 'kenapa', 'yaitu', 'yakni',
     'daripada', 'itulah', 'lagi', 'maka', 'tentang', 'demi', 'dimana',
     'kemana', 'pula', 'sambil', 'sebelum', 'sesudah', 'supaya', 'guna', 'kah',
     'pun', 'sampai', 'sedangkan', 'selagi', 'sementara', 'tetapi', 'apakah',
     'kecuali', 'sebab', 'selain', 'seolah', 'seraya', 'seterusnya', 'tanpa',
     'agak', 'boleh', 'dapat', 'dsb', 'dst', 'dll', 'dahulu', 'dulunya', 'anu',
     'demikian', 'tapi', 'ingin', 'juga', 'nggak', 'mari', 'nanti',
     'melainkan', 'oh', 'ok', 'seharusnya', 'sebetulnya', 'setiap',
     'setidaknya', 'sesuatu', 'pasti', 'saja', 'toh', 'ya', 'walau', 'tolong',
     'tentu', 'amat', 'apalagi', 'bagaimanapun', 'base64', 'no-repeat',
     'font-weight', 'font-size', 'uacvxbspan', 'wurvib', 'vertical-align',
     'background-color']


def stop_word_remover(text, library_ina):
    """Remove stop words."""
    words = text.split(' ')
    for word in words:
        if word in library_ina:
            words.remove(word)
    return ''.join(words)

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()
q_clean = 'bimajatiwijaya'
q_len = len(q_clean)
q = 'bima+jati+wijaya'
next_url = False
relevan = {}
total_relevan = 0
total_url = 0
precision = 75.0
# while True:
# SPLIT based <div class="g"> mean 1 document
path = '/home/bima/Github/pascasarjana/IR_TASK1/bing_data/html_page_'
for i in range(1, 72, 1):
    print(i, " : ", datetime.datetime.now())
    to_open = path + str(i) + '.txt'
    print(to_open)
    f = open(to_open)
    soup = BeautifulSoup(f.read(), 'html.parser')
    for i in soup.prettify().split('<li class="b_algo">'):
        element = BeautifulSoup(i, 'html.parser')
        link_found = element.findAll("div", {'class': 'b_algoheader'})
        if not link_found:
            continue
        else:
            tab = re.sub('\s+', '', element.text)
            tab = unicodedata.normalize('NFKD', tab).encode('ascii', 'ignore')
            tab = stemmer.stem(tab)
            tab = stop_word_remover(tab, library_ina)
            # print (tab)  # WITHOUT TAG html
            i = 0
            tab_len = len(tab)
            # if you want to see string after steming and swr
            # print(tab)
            max = tab_len - 1
            similar = False
            next = q_len
            while (next < max):
                diff = editdistance.eval(tab[i:next], q_clean)
                res_distance = ((q_len - diff) / float(q_len)) * 100
                if res_distance >= precision:
                    similar = True
                    break
                i += 1
                next += 1
            if similar:
                total_relevan += 1
            total_url += 1
            print("URL ke ", total_url , " -> hasil relevan sementara : ", total_relevan)
    print("END ", i, " : ", datetime.datetime.now())
print(datetime.datetime.now(), " FINISHED")
print ("TOTAL URL : ", total_url)
print ("TOTAL RELEVAN : ", total_relevan)

