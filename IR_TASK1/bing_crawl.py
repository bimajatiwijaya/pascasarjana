import requests
from bs4 import BeautifulSoup
import unicodedata

"""
b_algo document

next page class
sb_pagN sb_pagN_bp b_widePag sb_bp
"""

q = 'bima+jati+wijaya'
# Get google with my name
r = requests.session()
set_url = {}
main_url = 'https://www.bing.com'
head_url = 'https://www.bing.com/search?q=' + q + ''
response = r.get(head_url, stream=True)
soup = BeautifulSoup(response.text, 'html.parser')

next_url = set_url.copy()
history_url = set_url.copy()
page_idx = 1
page = '/home/bima/Github/pascasarjana/IR_TASK1/bing_data/'
while True:
    print("Creating file %s"% (page_idx,))
    file_name = "html_page_" + str(page_idx) + ".txt"
    f = open(page + file_name, "w+")
    content = unicodedata.normalize('NFKD', soup.prettify()).encode('ascii', 'ignore')
    f.write(content)
    set_url = {}
    next = []
    for i in soup.findAll("a", {"class": "sb_pagN", }):
        set_url[i.get(key='href')] = False
    if not set_url:
        break
    print (set_url)
    next_url = set_url.popitem()[0]
    response = r.get(main_url + next_url, stream=True)
    soup = BeautifulSoup(response.text, 'html.parser')
    page_idx += 1

print ("Downloaded %s pages."% (page_idx - 1,))