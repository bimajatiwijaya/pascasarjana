import editdistance
b = 'struktur organisasi 2013 2014 - hmti udinus hmti nus ac id ' \
    'struktur-organisasi-2013- 2014 cache periode 2013- 2014 ketua umum ' \
    'faid ari prasetyo sekretaris umum muchamad hasan bisri bendahara umum ' \
    'dewi astria bidang 1 ilmu tahu teknologi kepala bidang iptek nova ' \
    'kharisma husen sekretaris bidang iptek septi maulina departemen ' \
    'hardware bima jati w ryan yunus'
b_len = len(b)
q = 'bima jati wijaya'
q_len = len(q)
res = editdistance.eval(q, b)
i = 0
max = b_len-1
similar = False
next = i + q_len

while(next < max):
    next = i + q_len
    # to show result please remove comment on code bellow
    print(b[i:next], " vs ", q, "= ", editdistance.eval(b[i:next], q))
    diff = editdistance.eval(b[i:next], q)
    print(((q_len - diff) / float(q_len)) * 100)
    if ((q_len - diff) / float(q_len)) * 100 >= 75.0:
        similar = True
        break
    i += 1
print(similar)
