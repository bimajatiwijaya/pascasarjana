import requests
from bs4 import BeautifulSoup
import unicodedata

q = 'bima+jati+wijaya'
# Get google with my name
r = requests.session()
set_url = {}
main_url = 'https://www.google.co.id'
head_url = 'https://www.google.co.id/search?q=' + q + ''
response = r.get(head_url, stream=True)
soup = BeautifulSoup(response.text, 'html.parser')
for i in soup.findAll("a", {"class": "fl"}):
    set_url[i.get(key='href')] = False

next_url = set_url.copy()
history_url = set_url.copy()
page_idx = 1
page = '/home/bima/Github/pascasarjana/IR_TASK1/google_data/'
while True:
    print("Creating file %s"% (page_idx,))
    file_name = "html_page_" + str(page_idx) + ".txt"
    f = open(page + file_name, "w+")
    content = unicodedata.normalize('NFKD', soup.prettify()).encode('ascii', 'ignore')
    f.write(content)
    set_url = {}
    next = []
    for i in soup.findAll("a", {"class": "fl", }):
        next = i.findAll("span", {"style": "display:block;margin-left:53px", })
        if next:
            next = next[0]
        set_url[i.get(key='href')] = False
    if not set_url:
        break
    if not next:
        print(next)
        break
    next_url = set_url.popitem()[0]
    response = r.get(main_url + next_url, stream=True)
    soup = BeautifulSoup(response.text, 'html.parser')
    page_idx += 1

print ("Downloaded %s pages."% (page_idx - 1,))